package ru.nsu.belov.geotasks;

import android.os.Handler;

import java.util.ArrayList;

/**
 * Created by Evgeny on 24.12.2017.
 */

public class MyTimer {
    private static MyTimer timer;
    private static Handler timerHandler = new Handler();
    private static Runnable timerRunnable;
    private ArrayList<OnTimerClickListener> onTimerClickListeners = new ArrayList<>();
    private long startTime;

    private MyTimer(final int seconds) {
        startTime = System.currentTimeMillis();
        timerRunnable = new Runnable() {
            @Override
            public void run() {
                timerHand();
            }

            private void timerHand() {
                long millis = (1000 * seconds) - (System.currentTimeMillis() - startTime);
                int seconds = (int) (millis / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                if (seconds + minutes > 0) {
                    timerHandler.postDelayed(this, 1000);
                }
                for (OnTimerClickListener onTCL : onTimerClickListeners) {
                    if (onTCL != null) {
                        onTCL.onTimerClick(minutes, seconds);
                    }
                }
            }
        };
        timerHandler.postDelayed(timerRunnable, 0);

    }

    static MyTimer getNewTimer(int seconds, OnTimerClickListener onTimerClickListener) {

        if (timer == null) {
            timer = new MyTimer(seconds);
        }
        timer.onTimerClickListeners.add(onTimerClickListener);
        return timer;
    }

    public static void removeTimer() {
        if (timer != null) {
            timer.onTimerClickListeners.clear();
            timer = null;
        }
    }

    interface OnTimerClickListener {
        void onTimerClick(int minutes, int seconds);
    }
}
