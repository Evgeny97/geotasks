package ru.nsu.belov.geotasks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

public class MapActivity extends AppCompatActivity
        implements GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener, OnMapReadyCallback, MyTimer.OnTimerClickListener {

    RecordController recordController;
    private TextView scoreTextView;
    private TextView timeTextView;
    private boolean playTime = true;
    private int score = 0;
    private GoogleMap mMap;
    private LatLng place;
    private Geocoder geocoder;
    private Marker marker;

    static Intent getNewActivityIntent(Context context) {
        Intent intent = new Intent(context, MapActivity.class);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Locale.setDefault(Locale.ENGLISH);
        Configuration config = new Configuration();
        config.locale = Locale.ENGLISH;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_map);
        timeTextView = findViewById(R.id.time);
        scoreTextView = findViewById(R.id.score);
        Button streetButton = findViewById(R.id.streetButton);
        streetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(StreetViewActivity.getNewActivityIntent(MapActivity.this, place));
            }
        });

        findViewById(R.id.acceptButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptMarker();
            }
        });
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        place = Places.getRandomPlace();
        recordController = new RecordController(this);
        scoreTextView.setText("Score: " + score);
        geocoder = new Geocoder(this, Locale.UK);
        MyTimer timer = MyTimer.getNewTimer(80, this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMaxZoomPreference(25);
        map.getUiSettings().setCompassEnabled(false);
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
    }

    @Override
    public void onMapClick(LatLng point) {
        if (playTime) {
            if (marker != null) {
                marker.remove();
            }
            marker = mMap.addMarker(new MarkerOptions()
                    .position(point));

        }
    }

    private void acceptMarker() {
        try {
            if (marker != null) {
                LatLng tapPosition = marker.getPosition();
                float[] results = new float[1];
                Location.distanceBetween(tapPosition.latitude, tapPosition.longitude, place.latitude, place.longitude, results);
                int newScore = (int) (10000000 / results[0]);
                Toast.makeText(this, "+ " + newScore + " score", Toast.LENGTH_SHORT).show();
                score += newScore;
                scoreTextView.setText("Score: " + score);
                marker.remove();
                marker = null;
                place = Places.getRandomPlace();
            } else {
                Toast.makeText(this, "Choose place from street view", Toast.LENGTH_SHORT).show();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void showEndGameDialog() {
        try {
            if (recordController.isRecord(score)) {
                final EditText input = new EditText(this);
                input.setText("UserName");
                new AlertDialog.Builder(this)
                        .setTitle("Please enter your nickname")
                        .setView(input)
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                recordController.addRecord(input.getText().toString(), score);
                                showRecordsDialog();
                            }
                        })
                        .setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        //recordController.addRecord(input.getText().toString(), score);
                                        dialog.dismiss();
                                        //showRecordsDialog();
                                    }
                                }
                        )
                        .show();
            } else {
                showRecordsDialog();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void showRecordsDialog() {
        try {


            if (!isFinishing() && !isDestroyed()) {
                String recordsString = "Your score: " + score + "\n";
                String[] recordsArray = recordController.getRecords();
                for (String record : recordsArray) {
                    recordsString += record + "\n";
                }
                new AlertDialog.Builder(this)
                        .setTitle("Time is out")
                        .setMessage(recordsString)
                        .setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                }
                        ).setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        MapActivity.this.finish();
                    }
                })
                        .show();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
//        try {
//            List<Address> addresses = geocoder.getFromLocationName(place.getCapital(), 1);
//            if (addresses != null && !addresses.isEmpty()) {
//                moveCameraToTarget(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()));
//            }
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();
//        }
    }

    public void moveCameraToTarget(LatLng latLng) {
        CameraPosition target =
                new CameraPosition.Builder().target(latLng)
                        .zoom(9)
                        .bearing(0)
                        .tilt(25)
                        .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(target), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
//                Toast.makeText(getBaseContext(), "Animation complete", Toast.LENGTH_SHORT)
//                        .show();
            }

            @Override
            public void onCancel() {
//                Toast.makeText(getBaseContext(), "Animation canceled", Toast.LENGTH_SHORT)
//                        .show();
            }
        });
    }

    @Override
    public void onTimerClick(int minutes, int seconds) {
        timeTextView.setText(String.format("Time: %d:%02d", minutes, seconds));
        if (minutes + seconds <= 0) {
            if (playTime) {
                playTime = false;
                showEndGameDialog();
            }
        }
    }
}
