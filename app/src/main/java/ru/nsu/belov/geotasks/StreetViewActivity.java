package ru.nsu.belov.geotasks;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

public class StreetViewActivity extends FragmentActivity implements
        OnStreetViewPanoramaReadyCallback, MyTimer.OnTimerClickListener {

    private StreetViewPanorama streetViewPanorama;
    private TextView timeTextView;
    private LatLng latLng;

    static Intent getNewActivityIntent(Context context, LatLng latLng) {
        Intent intent = new Intent(context, StreetViewActivity.class);
        intent.putExtra("1", latLng.latitude);
        intent.putExtra("2", latLng.longitude);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_street_view);
        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
        timeTextView = findViewById(R.id.time);
        MyTimer.getNewTimer(60, this);
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        latLng = new LatLng(getIntent().getDoubleExtra("1", 0),
                getIntent().getDoubleExtra("2", 0));
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        this.streetViewPanorama = streetViewPanorama;
        streetViewPanorama.setPosition(latLng);
        streetViewPanorama.setUserNavigationEnabled(true);
    }

    @Override
    public void onTimerClick(int minutes, int seconds) {
        timeTextView.setText(String.format("Time: %d:%02d", minutes, seconds));
        if (minutes + seconds <= 0) {
            finish();
        }
    }
}
