package ru.nsu.belov.geotasks;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button startButton;
    Button recordsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startButton = findViewById(R.id.start_button);
        recordsButton = findViewById(R.id.show_records_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyTimer.removeTimer();
                startActivity(MapActivity.getNewActivityIntent(MainActivity.this));
            }
        });
        recordsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRecords();
            }
        });
    }

    void showRecords() {
        final RecordController recordController = new RecordController(this);

        String[] recordsArray = recordController.getRecords();
        String recordsString = "";
        for (String record : recordsArray) {
            recordsString += record + "\n";
        }
        new AlertDialog.Builder(this)
                .setTitle("Records:")
                .setMessage(recordsString)
                .setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }
                )
                .show();
    }

}
