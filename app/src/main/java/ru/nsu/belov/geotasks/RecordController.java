package ru.nsu.belov.geotasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Pair;

import java.util.ArrayList;

/**
 * Created by Evgeny on 09.12.2017.
 */

class RecordController {
    private final String preferencesKey = "RECORDS";
    private Context context;
    private ArrayList<Pair<String, Integer>> records = new ArrayList<>();

    RecordController(Context context) {
        this.context = context;
        load();
    }

    private void load() {
        SharedPreferences prefs = context.getSharedPreferences(preferencesKey, Context.MODE_PRIVATE);
        for (int i = 0; i < 3; i++) {
            records.add(new Pair<>(prefs.getString("Name" + i, "Empty"),
                    prefs.getInt("Score" + i, 0)));
        }
    }

    boolean isRecord(int score) {
        return score > records.get(2).second;
    }

    void addRecord(String name, int score) {
        Pair<String, Integer> record = new Pair<>(name, score);
        if (isRecord(score)) {
            if (score > records.get(0).second) {
                records.add(0, record);
            } else if (score > records.get(1).second) {
                records.add(1, record);
            } else {
                records.add(2, record);
            }
            records.remove(3);
            save();
        }
    }

    String[] getRecords() {
        String[] recordsArray = new String[3];
        for (int i = 0; i < 3; i++) {
            recordsArray[i] = records.get(i).first + ": " + records.get(i).second;
        }
        return recordsArray;
    }

    private void save() {
        SharedPreferences prefs = context.getSharedPreferences(preferencesKey, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        for (int i = 0; i < 3; i++) {
            Pair<String, Integer> record = records.get(i);
            ed.putString("Name" + i, record.first).putInt("Score" + i, record.second);
        }
        ed.apply();
    }
}
