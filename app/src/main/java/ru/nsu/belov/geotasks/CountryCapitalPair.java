package ru.nsu.belov.geotasks;

/**
 * Created by Evgeny on 01.12.2017.
 */

public class CountryCapitalPair {
    private String capital;
    private String country;

    public CountryCapitalPair(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCapital() {
        return capital;
    }

    public String getCountry() {
        return country;
    }
}
