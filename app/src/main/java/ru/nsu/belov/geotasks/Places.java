package ru.nsu.belov.geotasks;

import com.google.android.gms.maps.model.LatLng;

import java.util.Random;

/**
 * Created by Evgeny on 24.12.2017.
 */

public class Places {
    private static LatLng[] places = {
            new LatLng(55.7518282, 37.6133549),
            new LatLng(48.8583701, 2.2922926),
            new LatLng(43.7230588, 10.3977097),
            new LatLng(54.8426418,83.0921558),
    };
    private static Random r = new Random();


    public static LatLng getRandomPlace() {
        return places[r.nextInt(places.length)];
    }
}
